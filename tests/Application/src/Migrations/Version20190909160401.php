<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190909160401 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE banner_translation DROP FOREIGN KEY FK_841ECF1C2C2AC5D3');
        $this->addSql('ALTER TABLE banner_image DROP FOREIGN KEY FK_685235487E3C61F9');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA3944C3C583C9');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA394493CB796C');
        $this->addSql('ALTER TABLE page_translation DROP FOREIGN KEY FK_A3D51B1D2C2AC5D3');
        $this->addSql('ALTER TABLE product_group_product DROP FOREIGN KEY FK_8FA74C5335E4B3D0');
        $this->addSql('ALTER TABLE seo_translation DROP FOREIGN KEY FK_700E6CE32C2AC5D3');
        $this->addSql('ALTER TABLE slide_image DROP FOREIGN KEY FK_44B4D27C7E3C61F9');
        $this->addSql('ALTER TABLE webburza_article_product DROP FOREIGN KEY FK_2AB0C4534584665A');
        $this->addSql('ALTER TABLE webburza_article_translation DROP FOREIGN KEY FK_694DF1342C2AC5D3');
        $this->addSql('ALTER TABLE webburza_article DROP FOREIGN KEY FK_1F0572D212469DE2');
        $this->addSql('ALTER TABLE webburza_article_category_translation DROP FOREIGN KEY FK_341B128C2C2AC5D3');
        $this->addSql('ALTER TABLE webburza_article DROP FOREIGN KEY FK_1F0572D23DA5256D');
        $this->addSql('ALTER TABLE webburza_wishlist_item DROP FOREIGN KEY FK_65EB7091FB8E54CD');
        $this->addSql('CREATE TABLE sylius_shop_billing_data (id INT AUTO_INCREMENT NOT NULL, company VARCHAR(255) DEFAULT NULL, tax_id VARCHAR(255) DEFAULT NULL, country_code VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postcode VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sylius_avatar_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1068A3A97E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_avatar_image ADD CONSTRAINT FK_1068A3A97E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_admin_user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE banner');
        $this->addSql('DROP TABLE banner_image');
        $this->addSql('DROP TABLE banner_translation');
        $this->addSql('DROP TABLE customer_group');
        $this->addSql('DROP TABLE lexik_trans_unit');
        $this->addSql('DROP TABLE lexik_trans_unit_translations');
        $this->addSql('DROP TABLE lexik_translation_file');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE page_translation');
        $this->addSql('DROP TABLE product_group');
        $this->addSql('DROP TABLE product_group_product');
        $this->addSql('DROP TABLE seo');
        $this->addSql('DROP TABLE seo_translation');
        $this->addSql('DROP TABLE slide');
        $this->addSql('DROP TABLE slide_image');
        $this->addSql('DROP TABLE subscription');
        $this->addSql('DROP TABLE sylius_migrations');
        $this->addSql('DROP TABLE webburza_article');
        $this->addSql('DROP TABLE webburza_article_category');
        $this->addSql('DROP TABLE webburza_article_category_translation');
        $this->addSql('DROP TABLE webburza_article_image');
        $this->addSql('DROP TABLE webburza_article_product');
        $this->addSql('DROP TABLE webburza_article_translation');
        $this->addSql('DROP TABLE webburza_wishlist');
        $this->addSql('DROP TABLE webburza_wishlist_item');
        $this->addSql('ALTER TABLE sylius_taxon_translation DROP meta_keywords, DROP meta_description');
        $this->addSql('ALTER TABLE sylius_product DROP timesBought');
        $this->addSql('ALTER TABLE sylius_order_item ADD product_name VARCHAR(255) DEFAULT NULL, ADD variant_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_channel ADD shop_billing_data_id INT DEFAULT NULL, DROP showWholesaleInfo');
        $this->addSql('ALTER TABLE sylius_channel ADD CONSTRAINT FK_16C8119EB5282EDF FOREIGN KEY (shop_billing_data_id) REFERENCES sylius_shop_billing_data (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16C8119EB5282EDF ON sylius_channel (shop_billing_data_id)');
        $this->addSql('CREATE INDEX IDX_16C8119EE551C011 ON sylius_channel (hostname)');
        $this->addSql('ALTER TABLE sylius_shop_user CHANGE reg_number encoder_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_6196A1F9A393D2FB43625D9F ON sylius_order (state, updated_at)');
        $this->addSql('ALTER TABLE sylius_admin_user ADD encoder_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product_review CHANGE title title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_promotion_coupon ADD reusable_from_cancelled_orders TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE sylius_product_translation DROP enabled');
        $this->addSql('ALTER TABLE sylius_customer DROP reg_number, DROP company');
        $this->addSql('ALTER TABLE sylius_address DROP reg_number, DROP vat_company_number, DROP bank_account');
        $this->addSql('ALTER TABLE sylius_product_image DROP FOREIGN KEY FK_88C64B2D7E3C61F9');
        $this->addSql('ALTER TABLE sylius_product_image ADD CONSTRAINT FK_88C64B2D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_variant DROP quantity');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_channel DROP FOREIGN KEY FK_16C8119EB5282EDF');
        $this->addSql('CREATE TABLE banner (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_6F9DB8E777153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE banner_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_685235487E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE banner_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, link VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX banner_translation_uniq_trans (translatable_id, locale), INDEX IDX_841ECF1C2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE customer_group (customer_id INT NOT NULL, customergroupinterface_id INT NOT NULL, INDEX IDX_A3F531FE9395C3F3 (customer_id), INDEX IDX_A3F531FE696E6315 (customergroupinterface_id), PRIMARY KEY(customer_id, customergroupinterface_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE lexik_trans_unit (id INT AUTO_INCREMENT NOT NULL, key_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, domain VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX key_domain_idx (key_name, domain), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE lexik_trans_unit_translations (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, trans_unit_id INT DEFAULT NULL, locale VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, content LONGTEXT NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_B0AA3944C3C583C9 (trans_unit_id), UNIQUE INDEX trans_unit_locale_idx (trans_unit_id, locale), INDEX IDX_B0AA394493CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE lexik_translation_file (id INT AUTO_INCREMENT NOT NULL, domain VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, locale VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, extention VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, hash VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX hash_idx (hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, published TINYINT(1) NOT NULL, published_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_140AB62077153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE page_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, content TEXT DEFAULT NULL COLLATE utf8_unicode_ci, meta_keywords TEXT DEFAULT NULL COLLATE utf8_unicode_ci, meta_description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, active TINYINT(1) NOT NULL, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, custom TINYINT(1) NOT NULL, UNIQUE INDEX page_translation_uniq_trans (translatable_id, locale), INDEX IDX_A3D51B1D2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product_group (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_CC9C3F9977153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product_group_product (product_group_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_8FA74C5335E4B3D0 (product_group_id), UNIQUE INDEX UNIQ_8FA74C534584665A (product_id), PRIMARY KEY(product_group_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE seo (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_6C71EC3077153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE seo_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, meta_keywords TEXT DEFAULT NULL COLLATE utf8_unicode_ci, meta_description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX seo_translation_uniq_trans (translatable_id, locale), INDEX IDX_700E6CE32C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE slide (id INT AUTO_INCREMENT NOT NULL, published TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, position INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE slide_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_44B4D27C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE subscription (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, email VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE sylius_migrations (version VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, category_id INT DEFAULT NULL, published TINYINT(1) NOT NULL, featured TINYINT(1) NOT NULL, published_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_1F0572D23DA5256D (image_id), INDEX IDX_1F0572D212469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article_category (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, published TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article_category_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX webburza_article_category_translation_uniq_trans (translatable_id, locale), INDEX IDX_341B128C2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article_image (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article_product (product_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_2AB0C4534584665A (product_id), UNIQUE INDEX UNIQ_2AB0C4537294869C (article_id), PRIMARY KEY(product_id, article_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_article_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, lead TEXT DEFAULT NULL COLLATE utf8_unicode_ci, content TEXT DEFAULT NULL COLLATE utf8_unicode_ci, meta_keywords TEXT DEFAULT NULL COLLATE utf8_unicode_ci, meta_description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, active TINYINT(1) NOT NULL, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX webburza_article_translation_uniq_trans (translatable_id, locale), INDEX IDX_694DF1342C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_wishlist (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, public TINYINT(1) NOT NULL, description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_50F062D8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webburza_wishlist_item (id INT AUTO_INCREMENT NOT NULL, wishlist_id INT NOT NULL, product_variant_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_65EB7091FB8E54CD (wishlist_id), INDEX IDX_65EB7091A80EF684 (product_variant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE banner_image ADD CONSTRAINT FK_685235487E3C61F9 FOREIGN KEY (owner_id) REFERENCES banner_translation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE banner_translation ADD CONSTRAINT FK_841ECF1C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES banner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE customer_group ADD CONSTRAINT FK_A3F531FE696E6315 FOREIGN KEY (customergroupinterface_id) REFERENCES sylius_customer_group (id)');
        $this->addSql('ALTER TABLE customer_group ADD CONSTRAINT FK_A3F531FE9395C3F3 FOREIGN KEY (customer_id) REFERENCES sylius_customer (id)');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA394493CB796C FOREIGN KEY (file_id) REFERENCES lexik_translation_file (id)');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA3944C3C583C9 FOREIGN KEY (trans_unit_id) REFERENCES lexik_trans_unit (id)');
        $this->addSql('ALTER TABLE page_translation ADD CONSTRAINT FK_A3D51B1D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_group_product ADD CONSTRAINT FK_8FA74C5335E4B3D0 FOREIGN KEY (product_group_id) REFERENCES product_group (id)');
        $this->addSql('ALTER TABLE product_group_product ADD CONSTRAINT FK_8FA74C534584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seo_translation ADD CONSTRAINT FK_700E6CE32C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES seo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slide_image ADD CONSTRAINT FK_44B4D27C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES slide (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_article ADD CONSTRAINT FK_1F0572D212469DE2 FOREIGN KEY (category_id) REFERENCES webburza_article_category (id)');
        $this->addSql('ALTER TABLE webburza_article ADD CONSTRAINT FK_1F0572D23DA5256D FOREIGN KEY (image_id) REFERENCES webburza_article_image (id)');
        $this->addSql('ALTER TABLE webburza_article_category_translation ADD CONSTRAINT FK_341B128C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES webburza_article_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_article_product ADD CONSTRAINT FK_2AB0C4534584665A FOREIGN KEY (product_id) REFERENCES webburza_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_article_product ADD CONSTRAINT FK_2AB0C4537294869C FOREIGN KEY (article_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_article_translation ADD CONSTRAINT FK_694DF1342C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES webburza_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_wishlist ADD CONSTRAINT FK_50F062D8A76ED395 FOREIGN KEY (user_id) REFERENCES sylius_shop_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_wishlist_item ADD CONSTRAINT FK_65EB7091A80EF684 FOREIGN KEY (product_variant_id) REFERENCES sylius_product_variant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webburza_wishlist_item ADD CONSTRAINT FK_65EB7091FB8E54CD FOREIGN KEY (wishlist_id) REFERENCES webburza_wishlist (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE sylius_shop_billing_data');
        $this->addSql('DROP TABLE sylius_avatar_image');
        $this->addSql('ALTER TABLE sylius_address ADD reg_number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD vat_company_number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD bank_account VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE sylius_admin_user DROP encoder_name');
        $this->addSql('DROP INDEX UNIQ_16C8119EB5282EDF ON sylius_channel');
        $this->addSql('DROP INDEX IDX_16C8119EE551C011 ON sylius_channel');
        $this->addSql('ALTER TABLE sylius_channel ADD showWholesaleInfo TINYINT(1) NOT NULL, DROP shop_billing_data_id');
        $this->addSql('ALTER TABLE sylius_customer ADD reg_number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD company VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX IDX_6196A1F9A393D2FB43625D9F ON sylius_order');
        $this->addSql('ALTER TABLE sylius_order_item DROP product_name, DROP variant_name');
        $this->addSql('ALTER TABLE sylius_product ADD timesBought INT NOT NULL');
        $this->addSql('ALTER TABLE sylius_product_image DROP FOREIGN KEY FK_88C64B2D7E3C61F9');
        $this->addSql('ALTER TABLE sylius_product_image ADD CONSTRAINT FK_88C64B2D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_product_translation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_review CHANGE title title VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE sylius_product_translation ADD enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE sylius_product_variant ADD quantity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_promotion_coupon DROP reusable_from_cancelled_orders');
        $this->addSql('ALTER TABLE sylius_shop_user CHANGE encoder_name reg_number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE sylius_taxon_translation ADD meta_keywords TEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD meta_description TEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
