# Plugin Wunderbit Commerce

Start accepting bitcoin payments from Sylius platform

## Installation

1. Run `composer require wunderbit/sylius-wunderbit-commerce-plugin`.

2. Add our Bundle 

   
    1) If you use Symfony 4 add new row into the bundles.php file: 
    
        Wunderbit\SyliusWunderbitCommercePlugin\WunderbitSyliusWunderbitCommercePlugin::class => ['all' => true]
   
   
    2) If you use Symfony 3 add new row into the app/AppKernel.php $bundles array:
        class AppKernel extends Kernel
         {
             // ...
            
             public function registerBundles()
             {
                 $bundles = array(
                     // ...
                     new Wunderbit\SyliusWunderbitCommercePlugin\WunderbitSyliusWunderbitCommercePlugin(),
                 );
            
                 // ...
             }
         }
    
3. Run the following commands to install all assets and yarn:

    
     yarn build
     
     php bin/console a:i
     
     php bin/console cache:clear

Use our **thankYou.html.twig** (vendor/wunderbit/sylius-wunderbit-commerce-plugin/src/Resources/views/SyliusShopBundle/Order/thankYou.html.twig) **for auto redirect to invoice**    

## Usage

   Use our plugin to enable new payment method in Sylius Admin that allows your e-store create Wunderbit Commerce invoices.
   
   1. First of all create new Payment method that calls - Wunderbit Payment
   
   2. Secondary for start using our plugin you need to create [new merchant account](https://commerce.wunderbit.co/)
   and generate API key (Merchant cabinet -> Settings -> API).
   
   After that just copy your generated API key and paste it in API field in your Wunderbit Payment method
   
   Now you can successfully use our service for generating Crypto invoices
   
## Advanced

   Every kind of invoice need to special extra parameters, that you can overwrite as parameter with "capture.wunderbit.action.description" key
   
    1) XML example:
    
    <parameter key="capture.wunderbit.action.description" type="collection">
         <parameter key="Description">Wunderbit Commerce Sylius API</parameter>
         <parameter key="test">test</parameter>
         # Add as many parameters as you want
    </parameter>
    
    2) YAML example:
    
     capture.wunderbit.action.description:
        Description: 'Wunderbit Commerce Sylius API'
        test: 'test'
        # Add as many parameters as you want
   
   Also our bundle use default Symfony route parameters **(router.request_context.host and router.request_context.scheme)** that allows generate correct capture url to your site.
   
   By default Symfony set **router.request_context.host** as **'localhost'** and **router.request_context.scheme** as **'http'**.
   
#### Overwrite them to your host name and scheme!
