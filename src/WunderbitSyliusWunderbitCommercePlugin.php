<?php


namespace Wunderbit\SyliusWunderbitCommercePlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class WunderbitSyliusWunderbitCommercePlugin extends Bundle
{
    use SyliusPluginTrait;
}