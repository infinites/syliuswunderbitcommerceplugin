<?php

declare(strict_types=1);

namespace Wunderbit\SyliusWunderbitCommercePlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SyliusWunderbitGatewayConfigurationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('api_key', TextType::class, ['label' => 'Wunderbit Commerce Merchant API Key']);
    }

}