<?php

namespace Wunderbit\SyliusWunderbitCommercePlugin\Controller;

use Doctrine\ORM\EntityManager;
use Sylius\Component\Core\Model\Payment;
use Sylius\Component\Core\Model\PaymentInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WunderbitCallbackController extends AbstractController
{

    public function paidCallbackAction(Request $request)
    {
        /* @var $em EntityManager */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var Payment $payment
         */
        $payment = $em->getRepository(Payment::class)->findOneBy(['id' => $request->request->get('custom_id')]);
        $payment->setState(PaymentInterface::STATE_COMPLETED);
        $paymentState = $request->request->get('deviation_status');
        if ($paymentState == 'complete') {
            if ($request->request->get('status')) {
                $paymentState = $request->request->get('status');
            }
        }
        $payment->getOrder()->setPaymentState($paymentState);
        $em->persist($payment);
        $em->flush();
        return new Response('ok');
    }

}