<?php


namespace Wunderbit\SyliusWunderbitCommercePlugin\Payum\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use GuzzleHttp\Client;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Wbc\WbcSDK;
use Wunderbit\SyliusWunderbitCommercePlugin\Payum\SyliusWunderbitApi;
use Payum\Core\Request\Capture;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Exception\RequestNotSupportedException;

class CaptureWunderbitAction implements ActionInterface, ApiAwareInterface
{
    /** @var Client */
    private $client;
    /** @var SyliusWunderbitApi */
    private $api;

    private $description;

    private $host;

    private $scheme;

    /** @var LocaleContextInterface */
    private $locale;

    public function __construct(Client $client, $description, $host, $scheme, LocaleContextInterface $locale)
    {
        $this->client = $client;
        $this->description = $description;
        $this->host = $host;
        $this->scheme = $scheme;
        $this->locale = $locale;
    }

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var SyliusPaymentInterface $payment */
        $payment = $request->getModel();

        $items = [];
        foreach ($payment->getOrder()->getItems() as $item) {
            $items[] = [
                'item_id' => $item->getId(),
                'item_name' => $item->getProduct()->getName(),
                'item_price' => $item->getTotal() / 100,
                'item_amount' => $item->getQuantity()
            ];
        }

        $wbcSDK = new WbcSDK($this->api->getApiKey());

        $comebackUrl = $this->scheme.'://'.$this->host.'/'.$this->locale->getLocaleCode();
        $invoice = $wbcSDK->generateInvoice(
            $payment->getOrder()->getCustomer()->getEmail(),
            $payment->getOrder()->getTotal() / 100,
            $payment->getId(),
            $items,
            $this->description,
            $comebackUrl,
            $comebackUrl.'/api/def/check_paid_callback'
        );
        $payment->setDetails(['status' => $invoice->status, 'redirect_wunderbit_url' => $wbcSDK->getInvoiceUrl($invoice->data->invoice_code)]);
    }

    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof SyliusPaymentInterface
            ;
    }

    public function setApi($api): void
    {
        if (!$api instanceof SyliusWunderbitApi) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . SyliusWunderbitApi::class);
        }

        $this->api = $api;
    }

}