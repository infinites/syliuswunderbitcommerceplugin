<?php

declare(strict_types=1);

namespace Wunderbit\SyliusWunderbitCommercePlugin\Payum;

use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;
use Wunderbit\SyliusWunderbitCommercePlugin\Payum\Action\StatusWunderbitAction;

class SyliusWunderbitPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'wunderbit_payment',
            'payum.factory_title' => 'Wunderbit Payment',
            'payum.action.status' => new StatusWunderbitAction(),
        ]);
        $config['payum.api'] = function (ArrayObject $config) {
            return new SyliusWunderbitApi($config['api_key']);
        };
    }
}